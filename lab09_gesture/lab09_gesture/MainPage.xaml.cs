﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace lab09_gesture
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            TapDemo.Clicked += async (sender, e) =>
            {
                await Navigation.PushAsync(new Tap());
            };

            PinchDemo.Clicked += async (sender, e) =>
            {
                await Navigation.PushAsync(new PinchDemo());
            };

            PanDemo.Clicked += async (sender, e) =>
            {
                await Navigation.PushAsync(new PanDemo());
            };

            SwipeDemo.Clicked += async (sender, e) =>
            {
                await Navigation.PushAsync(new SwipeDemo());
            };
        }
    }
}
