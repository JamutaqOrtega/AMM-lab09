﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace lab09_gesture
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Tap : ContentPage
    {
        int tapCount;
        readonly Label label;

        public Tap()
        {
            InitializeComponent();
            var image = new Image
            {
                Source = "docker.png",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            };

            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.NumberOfTapsRequired = 2;
            tapGestureRecognizer.Tapped += ObTapGestureRecognizerTapped;
            image.GestureRecognizers.Add(tapGestureRecognizer);

            label = new Label
            {
                Text = "Tap the photo",
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            Content = new StackLayout
            {
                Padding = new Thickness(20, 100),
                Children =
                {
                    image,
                    label
                }
            };
        }

        void ObTapGestureRecognizerTapped(object senderm, EventArgs args)
        {
            tapCount++;
            label.Text = String.Format("{0} tap{1} so far!",
                tapCount,
                tapCount == 1 ? "" : "s");

            var imageSender = (Image)senderm;

            //cambia la imgen de docker a debian
            if (tapCount % 2 == 0)
            {
                imageSender.Source = "docker.png";
            }
            else
            {
                imageSender.Source = "debian.png";
            }
        }
    }
}